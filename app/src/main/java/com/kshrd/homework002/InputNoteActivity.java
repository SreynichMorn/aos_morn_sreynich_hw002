package com.kshrd.homework002;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import org.json.JSONObject;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.PriorityQueue;

public class InputNoteActivity extends AppCompatActivity {
    EditText editTitle,editContent;
    ListView listView;
    ArrayList<String>arrayList;
    ArrayAdapter<String>adapter;
    PriorityQueue<String> hs;

    JSONObject save=new JSONObject();

    public static final String mypreference = "mypref";
    public static final String Title = "titleKey";
    public static final String Content = "contentKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_note);
        editTitle = findViewById(R.id.editTitle);
        editContent =  findViewById(R.id.editContent);
        listView=findViewById(R.id.listView);

        arrayList=new ArrayList<String>();

        findViewById(R.id.btnSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(InputNoteActivity.this,NoteBookActivity.class);
//                startActivity(intent);

                if (!editTitle.getText().toString().isEmpty()&&!editContent.getText().toString().isEmpty()) {
                    arrayList.add(editTitle.getText().toString());
                    arrayList.add(editContent.getText().toString());
                    Deque<String> hs = new ArrayDeque<String>();
                    hs.addAll(arrayList);
                    arrayList.clear();
                    arrayList.addAll(hs);
                    adapter= new ArrayAdapter(InputNoteActivity.this, android.R.layout.simple_list_item_1, arrayList);
                    listView.setAdapter(adapter);
                    Toast.makeText(InputNoteActivity.this, "Inserted", Toast.LENGTH_LONG).show();
                } else {
                    editTitle.setError("Enter Title");
                    editContent.setError("Enter Content");
                }
            }
        });


        SharedPreferences sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Title)) {
            editTitle.setText(sharedpreferences.getString(Title, ""));
        }
        if (sharedpreferences.contains(Content)) {
            editContent.setText(sharedpreferences.getString(Content, ""));

        }

    }


    public void Clear(View view) {
        editTitle =  findViewById(R.id.editTitle);
        editContent = findViewById(R.id.editContent);
        editTitle.setText("");
        editContent.setText("");

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 1) {
//            if (resultCode == NoteBookActivity.RESULT_OK) {
//                String result = data.getStringExtra("newSubject");
//                //Add this value to your adapter and call notifyDataSetChanged();
//            }
//
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
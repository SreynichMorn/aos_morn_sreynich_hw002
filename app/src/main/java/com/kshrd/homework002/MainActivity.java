package com.kshrd.homework002;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clickGrallary(View view) {
        Intent intent=new Intent(MainActivity.this,GrallaryActivity.class);
        startActivity(intent);
    }

    public void onContact(View view) {
        Intent intent=new Intent(MainActivity.this,ContactActivity.class);
        startActivity(intent);
    }

    public void onCall(View view) {
        Intent intent=new Intent(MainActivity.this,CallActivity.class);
        startActivity(intent);
    }

    public void onNoteBook(View view) {
        Intent intent=new Intent(MainActivity.this,NoteBookActivity.class);
        startActivity(intent);
    }

    public void onProfile(View view) {
        Intent intent=new Intent(MainActivity.this,ProfileActivity.class);
        startActivity(intent);
    }
}
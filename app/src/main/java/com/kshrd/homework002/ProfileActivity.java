package com.kshrd.homework002;

import androidx.appcompat.app.AppCompatActivity;

import android.accessibilityservice.GestureDescription;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class ProfileActivity extends AppCompatActivity {
    DatePickerDialog picker;
    EditText editDate,name;
    Button btnDate;
    Spinner spinnerSub;
    RadioButton radioFemale,radioMale;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        String[] subject = {"Android Developer","IOS Developer","React Native Developer",
                    "Web Developer","Networking"};
        spinnerSub = findViewById(R.id.spinner1);
        final ArrayAdapter<String> dateAdapter = new ArrayAdapter<>(
                ProfileActivity.this,
                android.R.layout.simple_spinner_dropdown_item, subject
        );
        spinnerSub.setAdapter(dateAdapter);
        spinnerSub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        editDate=findViewById(R.id.editDate);
        btnDate=findViewById(R.id.btnDate);
        editDate.setInputType(InputType.TYPE_NULL);
        btnDate.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                picker = new DatePickerDialog(ProfileActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        editDate.setText(dayOfMonth +"/"+month+"/"+year);
                    }
                },year,month,day);
                picker.show();
            }
        });

    }

    public void onSave(View view) {
        editDate=findViewById(R.id.editDate);
        name=findViewById(R.id.name);
        radioFemale=findViewById(R.id.radioFemale);
        radioMale=findViewById(R.id.radioMale);
        spinnerSub=findViewById(R.id.spinner1);
        String message=editDate.getText().toString();
        String mes=name.getText().toString();
        String subject=spinnerSub.getSelectedItem().toString();
        String g=null;
        RadioGroup gender=findViewById(R.id.radioGroup);

        switch (gender.getCheckedRadioButtonId()){
            case R.id.radioFemale:
                g="Female";
                break;
            case R.id.radioMale:
                g="Male";
                break;
        }

        String total="Name:"+mes+"\nDate:"+message+"\nGender:"+g+"\nSubject:"+subject;
        Toast.makeText(ProfileActivity.this,total,Toast.LENGTH_LONG).show();
    }
}